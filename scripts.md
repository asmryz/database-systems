```
BEGIN
    FOR s IN (SELECT regno FROM ASIM.students WHERE sec = 'A') LOOP
        EXECUTE IMMEDIATE 'CREATE USER ' || s.regno || ' IDENTIFIED BY lab123';
        EXECUTE IMMEDIATE 'GRANT CONNECT,RESOURCE,UNLIMITED TABLESPACE TO ' || s.regno;
    END LOOP;
END;
```
```
BEGIN
    FOR s IN (SELECT regno FROM ASIM.students) LOOP
        EXECUTE IMMEDIATE 'DROP USER ' || s.regno;
    END LOOP;
END;
```

https://stackoverflow.com/questions/41718676/how-to-execute-sql-script-in-multiple-users-schema-oracle
